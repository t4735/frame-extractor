Depends on ffmpeg. ffmpeg must be in your path.

Usage: python frame_extractor.py -p {path to video files} -t {timestamp in hh:mm:ss format} -o {path to save frames}
