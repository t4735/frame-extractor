#!/usr/bin/env python

import os
import sys
import contextlib
import subprocess
from hashlib import md5
import concurrent.futures

@contextlib.contextmanager
def Path(tmp_dir):
    start_dir = os.getcwd()
    os.chdir(tmp_dir)
    yield
    os.chdir(start_dir)

def parse_args(args):
    _ = args.pop(0)
    while args:
        arg = args.pop(0)
        if arg == '-p':
            path = args.pop(0)
        elif arg == '-t':
            timecode = args.pop(0)
        elif arg == '-o':
            output = args.pop(0)
        else:
            print(f'Unknown argument: {arg}')
    return path,timecode,output

def isVideo(f):
    ext = f.split('.')[-1]
    formats = set(['mp4','mkv','m2ts','avi'])
    if ext in formats:
        return True
    return False

def getFrame(path, timecode):
    s = path+timecode
    _ = subprocess.run(['ffmpeg',
                    '-hide_banner',
                    '-loglevel', 'panic',
                    '-ss',f'{timecode}',
                    '-i', f'{path}',
                    '-vframes','1',
                    '-q:v','2',
                    f'{md5(s.encode()).hexdigest()}.jpg'])
    return None

path,timecode,output = parse_args(sys.argv)

with Path(path):
    paths = []
    for path,_,files in os.walk(os.getcwd()):
        for f in files:
            if isVideo(f):
                p = f'{path}/{f}'
                paths.append(p)

print('Paths Found. Generating Frames...')


with Path(output):
    with concurrent.futures.ThreadPoolExecutor() as ex:
        r = [ex.submit(getFrame, p, timecode) for p in paths]

        for i,__ in enumerate(concurrent.futures.as_completed(r)):
            print(f'\rProgress: {i+1}/{len(paths)}',end='')

print('\nFrames Generated')
